''' Rock Siccors Papper Game'''
import random
import sys
import pyfiglet


def banner():
    game_banner = pyfiglet.figlet_format("Welcome to Rock Paper Scissors Game!!!")
    print(game_banner)

def play():
    '''Core Game'''
    human_counter = 0
    computer_counter = 0
    while human_counter <= 2 and computer_counter <= 2:
        play_list = ["r","p","s"]
        human_input = input("Please type rock for r,scissor for s papper for p and type q \
for exit: ")
        computer_choice = random.choice(play_list)
        if human_input.lower()  in play_list:
            if  (computer_choice == "r" and human_input.lower() == "s") or \
                (computer_choice == "s" and human_input.lower() == "p") or \
                (computer_choice == "p" and human_input.lower() == "r"):
                print("Computer gain 1 point")
                computer_counter += 1
                if computer_counter == 3:
                    print("Computer Wins with " + str(computer_counter) + "s")
            elif computer_choice ==  human_input.lower():
                print("Draw!!!")
            else:
                print("Human gain 1 point")
                human_counter += 1
                if human_counter == 3:
                    print("Human Wins with " + str(human_counter) + "s")
        elif human_input.lower() == "q":
            sys.exit()
        else:
            print("Your choice is not correct!!!!")

if __name__ == "__main__":
    banner()
    play()
